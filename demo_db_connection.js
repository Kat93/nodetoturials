var mysql = require('mysql');

var con = mysql.createConnection({
    host: 'localhost',
    user: 'test_user',
    password: '1337Hest',
    port: 3306
});

con.connect(function(err) {
    if (err) throw err; 
    console.log('connected');

    con.query("CREATE DATABASE myfirstnodedb", function(err, result){
        if (err) throw err;
        console.log("Database created");
    });

});